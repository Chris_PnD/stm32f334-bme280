import tkinter as tk
from tkinter import ttk, Tk
from matplotlib.backends.backend_tkagg import (FigureCanvas)
from matplotlib.figure import Figure

from typing import List, Any

import serial
import serial.tools.list_ports

import threading
import time
import re

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

def read_from_port(s, y_temp, y_pres, y_humi, data_temp, data_pres, data_humi):

    while(True):
        serial_str = s.readline().decode('utf-8')
        if len(serial_str) > 0:
            temperature_str = re.findall("[+-]?\d+\.\d+ deg C", serial_str)
            pressure_str = re.findall("[+-]?\d+\.\d+ hPa", serial_str)
            humidity_str = re.findall("[+-]?\d+\.\d+%", serial_str)

            if len(temperature_str) > 0:
                float_temp_str = temperature_str[0].split(' ')
                if isfloat(float_temp_str[0]) is True:
                    temp = float(float_temp_str[0])
                    y_temp.append(temp)
                    x_temp = range(len(y_temp))
                    print("Temperature:{}".format(temp))
                if len(y_temp) > 0:
                    data_temp.clear()
                    data_temp.plot(x_temp, y_temp)

            if len(pressure_str) > 0:
                float_pres_str = pressure_str[0].split(' ')
                if isfloat(float_pres_str[0]) is True:
                    pres = float(float_pres_str[0])
                    y_pres.append(pres)
                    x_pres = range(len(y_pres))
                    print("Pressure:{}".format(pres))
                if len(y_pres) > 0:
                    data_pres.clear()
                    data_pres.plot(x_pres, y_pres)

            if len(humidity_str) > 0:
                float_humi_str = humidity_str[0].split('%')
                if isfloat(float_humi_str[0]) is True:
                    humi = float(float_humi_str[0])
                    y_humi.append(humi)
                    x_humi = range(len(y_humi))
                    print("Humidity:{}".format(humi))
                if len(y_humi) > 0:
                    data_humi.clear()
                    data_humi.plot(x_humi, y_humi)

            data_temp.set_title('Temperature [°C]')
            data_pres.set_title('Pressure [°C]')
            data_humi.set_title('Humidity [°C]')
            canvas.draw()
    s.close();
  
def quit_app():
    root.quit()  # stops main loop
    root.destroy()  # this is necessary on Windows to prevent


def read_start():
    s = serial.Serial(port=combobox1.get(), baudrate=38400)
    if s.isOpen():
        print("Open serial port")
        s.write(b'start 500\n')
        print(s.readline())
        print("Write start BME280 to serial port")   
        thread = threading.Thread(target=read_from_port, args=(s, y_temp, y_pres, y_humi, data_temp, data_pres, data_humi))
        #Daemonic threads can’t be joined. However, they are destroyed automatically when the main thread terminates.
        thread.daemon = True
        thread.start()

def read_stop():
    s = serial.Serial(port=combobox1.get(), baudrate=38400)
    if s.isOpen():
        print("Open serial port")
        s.write(b'stop\n')
        print(s.readline())
        print("Write stop BME280 to serial port")   

def led():
    s = serial.Serial(port=combobox1.get(), baudrate=38400)
    if s.isOpen():
        if (var1.get() == 1):
            s.write(b'led 100\n')
            print("Led blinking")
        else:    
            s.write(b'led 0\n')
            print("Led stop blinking")

root = Tk = tk.Tk()
root.wm_title("BME280 data viewer")

fig = Figure(figsize=(10, 2), dpi=80)
data_temp = fig.add_subplot(131)
data_pres = fig.add_subplot(132)
data_humi = fig.add_subplot(133)

data_temp.set_title('Temperature [°C]')
data_pres.set_title('Pressure [°C]')
data_humi.set_title('Humidity [°C]')

canvas = FigureCanvas(fig, master=root)
canvas.draw()
canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

y_temp = []
y_pres = []
y_humi = []

button3 = tk.Button(master=root, text="QUIT", command=quit_app)
button3.pack(side=tk.BOTTOM)

button2 = tk.Button(master=root, text="Stop Read Data", command=read_stop)
button2.pack(side=tk.BOTTOM)

button1 = tk.Button(master=root, text="Start Read Data", command=read_start)
button1.pack(side=tk.BOTTOM)

var1 = tk.IntVar(value=1)
checkbox1 = tk.Checkbutton(master=root, text='Nucleo user LED', variable=var1, onvalue=1, offvalue=0, command=led)
checkbox1.pack(side=tk.BOTTOM)

combobox1 = ttk.Combobox(root)
combobox1.pack(side=tk.BOTTOM)

ports = serial.tools.list_ports.comports()
list_cb = []
for port, desc, hwid in sorted(ports):
    list_cb.append(port)
    print("{}: {} [{}]".format(port, desc, hwid))

combobox1['values'] = list_cb
combobox1.set(port)

tk.mainloop()
