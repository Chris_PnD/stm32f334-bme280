/*
 * events.c
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#include "stdint.h"
#include "stdbool.h"
#include "events.h"
#include "misc.h"
/* USER CODE BEGIN PV */

volatile uint8_t event;


/* Functions ----------------------------------------------------------*/

void set_event(uint8_t event_type)
{
	EnterCritical();
	event |= event_type;
	ExitCritical();
}

void clear_event(uint8_t event_type)
{
	EnterCritical();
	event &= ~(event_type);
	ExitCritical();
}

bool check_event(uint8_t event_type)
{
	EnterCritical();
	bool ret = ((event & event_type) == event_type);
	ExitCritical();

	return ret;
}
