/*
 * bme280_middleware.c
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "main.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "../../Drivers/BME280_driver/bme280.h"
#include "cmsis_os.h"
#include "events.h"
#include "queues.h"

/* USER CODE BEGIN PTD */
/* Structure that contains identifier details used in example */
struct identifier
{
    /* Variable to hold device address */
    uint8_t dev_addr;
};

/* USER CODE BEGIN PV */
struct bme280_dev dev;
struct identifier id;

volatile bool i2c1_rxCompleteFlag = true;
volatile bool i2c1_txCompleteFlag = true;

extern I2C_HandleTypeDef hi2c1;
extern UART_HandleTypeDef huart2;
extern osEventFlagsId_t bme280EvtId;

/* Functions ----------------------------------------------------------*/


void bme280_delay_us(uint32_t period, void *intf_ptr)
{
	HAL_Delay(period / 1000);
}

BME280_INTF_RET_TYPE bme280_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr)
{
	struct identifier id;

	id = *((struct identifier *)intf_ptr);


	i2c1_rxCompleteFlag = false;
	if (HAL_OK == HAL_I2C_Mem_Read_IT(&hi2c1, (id.dev_addr << 1), reg_addr, sizeof(reg_addr), reg_data, len))
	{
		while (i2c1_rxCompleteFlag == false);

		return BME280_OK;
	}
	else
	{
		return BME280_E_COMM_FAIL;
	}
}
BME280_INTF_RET_TYPE bme280_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t len,
                                                    void *intf_ptr)
{
	struct identifier id;

	id = *((struct identifier *)intf_ptr);

	i2c1_txCompleteFlag = false;
	if (HAL_OK == HAL_I2C_Mem_Write_IT(&hi2c1, (id.dev_addr << 1), reg_addr, sizeof(reg_addr), (uint8_t *)reg_data, len))
	{
		while (i2c1_txCompleteFlag == false);
		return BME280_OK;
	}
	else
	{
		return BME280_E_COMM_FAIL;
	}
}

void print_sensor_data(struct bme280_data *comp_data_pt)
{
	char buf[QUEUE_MAX_MSG_LEN];
    float temp, press, hum;

#ifdef BME280_FLOAT_ENABLE
    temp = comp_data_pt->temperature;
    press = 0.01 * comp_data_pt->pressure;
    hum = comp_data_pt->humidity;
#else
#ifdef BME280_64BIT_ENABLE
    temp = 0.01f * comp_data_pt->temperature;
    press = 0.0001f * comp_data_pt->pressure;
    hum = 1.0f / 1024.0f * comp_data_pt->humidity;
#else
    temp = 0.01f * comp_data_pt->temperature;
    press = 0.01f * comp_data_pt->pressure;
    hum = 1.0f / 1024.0f * comp_data_pt->humidity;
#endif
#endif
    sprintf(buf, "%0.2lf deg C, %0.2lf hPa, %0.2lf%%\n", temp, press, hum);
    uart_queue_push(buf, strlen(buf));
}

int8_t main_bme280_init(void)
{
	HAL_UART_Transmit(&huart2, (uint8_t *)"Init\n", 5, 100);
	uint8_t DevAddress = (BME280_I2C_ADDR_PRIM << 1);
	if (HAL_OK == HAL_I2C_IsDeviceReady(&hi2c1, DevAddress, 3, 100))
	{
		int8_t ret;
		uint8_t id_address = 0xD0;
		uint8_t data_out;
		char buf[30];

		HAL_UART_Transmit(&huart2, (uint8_t *)"BME280-OK\n", 10, 100);

		HAL_I2C_Mem_Read(&hi2c1, DevAddress, id_address, sizeof(id_address), &data_out, sizeof(data_out), 100);

		sprintf(buf, "Mem at 0x%x = 0x%x\n", id_address, data_out);
		HAL_UART_Transmit(&huart2, (uint8_t *)buf, strlen(buf), 100);

		id.dev_addr = BME280_I2C_ADDR_PRIM;
		dev.intf_ptr = &id;
		dev.intf = BME280_I2C_INTF;
		dev.delay_us = bme280_delay_us;
		dev.read = bme280_read;
		dev.write = bme280_write;

		ret = bme280_init(&dev);
		if (ret != BME280_OK)
		{
		  sprintf(buf, "BME280 init Err:%d\n", ret);
		  HAL_UART_Transmit(&huart2, (uint8_t *)buf, strlen(buf), 100);
		  return ret;
		}
		else
		{
		  sprintf(buf, "BME280 init OK\n");
		  HAL_UART_Transmit(&huart2, (uint8_t *)buf, strlen(buf), 100);
		}

		/* Recommended mode of operation: Indoor navigation */
		dev.settings.osr_h = BME280_OVERSAMPLING_1X;
		dev.settings.osr_p = BME280_OVERSAMPLING_16X;
		dev.settings.osr_t = BME280_OVERSAMPLING_2X;
		dev.settings.filter = BME280_FILTER_COEFF_16;

		uint8_t settings_sel = BME280_OSR_PRESS_SEL | BME280_OSR_TEMP_SEL | BME280_OSR_HUM_SEL | BME280_FILTER_SEL;

		ret = bme280_set_sensor_settings(settings_sel, &dev);
		if (ret != BME280_OK)
		{
		  sprintf(buf, "BME280 settings Err:%d\n", ret);
		  HAL_UART_Transmit(&huart2, (uint8_t *)buf, strlen(buf), 100);
		  return ret;
		}
		else
		{
		  sprintf(buf, "BME280 settings OK\n");
		  HAL_UART_Transmit(&huart2, (uint8_t *)buf, strlen(buf), 100);
		  return ret;
		}
	}
	return BME280_E_DEV_NOT_FOUND;
}

bool bme280_trigger(void)
{
	char buf[QUEUE_MAX_MSG_LEN];
	int8_t ret;

	ret = bme280_set_sensor_mode(BME280_FORCED_MODE, &dev);
	if (ret != BME280_OK)
	{
	  sprintf(buf, "BME280 forced mode Err:%d\n", ret);
	  uart_queue_push(buf, strlen(buf));
	  return false;
	}
	else
	{
	  sprintf(buf, "BME280 forced mode OK - task scheduling\n");
	  uart_queue_push(buf, strlen(buf));
	  return true;
	}
}

#ifdef USE_FREERTOS_TIMERS
void bme280_set_event_trigger(void *argument)
{
#ifdef USE_FREERTOS_EVENT
	osEventFlagsSet(bme280EvtId, BME280_FORCE_MEASSURE_EVENT);
#else
	set_event(BME280_FORCE_MEASSURE_EVENT);
#endif
}

void bme280_set_event_data_read(void *argument)
{
#ifdef USE_FREERTOS_EVENT
	osEventFlagsSet(bme280EvtId, BME280_DATA_READ_EVENT);
#else
	set_event(BME280_DATA_READ_EVENT);
#endif
}
#else

void bme280_set_event_trigger(void)
{
	set_event(BME280_FORCE_MEASSURE_EVENT);
}

void bme280_set_event_data_read(void)
{
	set_event(BME280_DATA_READ_EVENT);
}
#endif

bool bme280_data_read(struct bme280_data* comp_data_pt)
{
	int8_t ret;
	char buf[QUEUE_MAX_MSG_LEN];

	ret = bme280_get_sensor_data(BME280_ALL, comp_data_pt, &dev);
	if (ret != BME280_OK)
	{
		sprintf(buf, "BME280 data read Err:%d\n", ret);
		uart_queue_push(buf, strlen(buf));
		return false;
	}
	else
	{
		return true;
	}
}

void i2c1_rxComplete(I2C_HandleTypeDef *hi2c)
{
	i2c1_rxCompleteFlag = true;
}

void i2c1_txComplete(I2C_HandleTypeDef *hi2c)
{
	i2c1_txCompleteFlag = true;
}

