/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "cmsis_os.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "stdbool.h"
#include "bme280_middleware.h"
#include "cmsis_os2.h"
#include "events.h"
#include "queues.h"
#include "task.h"
#include "timers.h"

#ifndef USE_FREERTOS_TIMERS
#include "scheduler.h"
#endif

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

typedef enum {
	UART_IDLE,
	UART_TRANSMIT,
	UART_TRANSMITED,
} uart_state_t;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
I2C_HandleTypeDef hi2c1;

TIM_HandleTypeDef htim6;

UART_HandleTypeDef huart2;

/* Definitions for defaultTask */
osThreadId_t defaultTaskHandle;
const osThreadAttr_t defaultTask_attributes = {
  .name = "defaultTask",
  .stack_size = 200 * 4,
  .priority = (osPriority_t) osPriorityNormal,
};
/* USER CODE BEGIN PV */
osThreadId_t uartTaskHandle;
const osThreadAttr_t uartTask_attributes = {
  .name = "uartTask",
  .stack_size = 128 * 4,
  .priority = (osPriority_t) osPriorityBelowNormal,
};

volatile uart_state_t uartTxState = UART_IDLE;

#ifdef USE_FREERTOS_TIMERS
osTimerId_t ledTimerId;
osTimerId_t bme280SetEventTriggerTimerId;
osTimerId_t bme280SetEventDataReadTimerId;
#endif

#ifdef USE_FREERTOS_QUEUE
// uart message rtos queue id
osMessageQueueId_t uartQueue;

#ifdef USE_FREERTOS_MEMPOOL
osMemoryPoolId_t MemPoolId;
#endif //USE_FREERTOS_MEMPOOL

#endif //USE_FREERTOS_QUEUE

uint8_t rxData;

const char *commands_tab[] = {"start", "stop", "led", };
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C1_Init(void);
static void MX_USART2_UART_Init(void);
static void MX_TIM6_Init(void);
void StartDefaultTask(void *argument);

/* USER CODE BEGIN PFP */
static void uartTask(void *argument);

#ifndef USE_FREERTOS_TIMERS
static void led_blink(void);
#endif

static void event_processing(void);
static void queue_processing(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

#ifdef USE_FREERTOS_TIMERS
static void ledTimerCb(void *argument)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
}
#else
static void led_blink(void)
{
	HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
}
#endif

#ifdef USE_FREERTOS_EVENT
osEventFlagsId_t bme280EvtId;
#endif

void uart_txComplete(UART_HandleTypeDef *huart)
{
#ifdef USE_FREERTOS_QUEUE
	xTaskResumeFromISR(uartTaskHandle);
	vTaskMissedYield();
#else
	uartTxState = UART_TRANSMITED;
#endif
}

static char *uart_rxProcess(char data)
{
	static char cmd_st[MAX_UART_COMMAND_LEN];
	static uint8_t cmd_idx = 0;

	if (data == '\n' || data == '\r' || cmd_idx >= MAX_UART_COMMAND_LEN )
	{
		cmd_st[cmd_idx] = 0x00;  	// terminating null adding to the end of string
		cmd_idx = 0;
		return cmd_st;				// command completed
	}
	else
	{
		cmd_st[cmd_idx++] = data;  	// store command string
		return NULL;				// null - command not completed
	}
}

static void uart_cmdParsingFromISR(char* string_pt)
{
	for (uint8_t i = 0; i < sizeof(commands_tab)/sizeof(commands_tab[0]); i++)
	{
		if (0 == memcmp(string_pt, commands_tab[i], strlen(commands_tab[i])))
		{
			BaseType_t xHigherPriorityTaskWoken = pdFALSE;

			switch(i)
			{
			case 0:			// start
			{
				int val = atoi(string_pt + strlen(commands_tab[i]));
				if (val > 0)
				{
					if (pdPASS == xTimerChangePeriodFromISR(bme280SetEventTriggerTimerId, val, &xHigherPriorityTaskWoken))
					{
						uart_queue_push("BME280 start\n", 13);
					}
				}
				else
				{
					if (pdPASS == xTimerStopFromISR(bme280SetEventTriggerTimerId, &xHigherPriorityTaskWoken))
					{

						uart_queue_push("BME280 stop\n", 12);
					}
				}
				break;
			}
			case 1:			//stop
				if (pdPASS == xTimerStopFromISR(bme280SetEventTriggerTimerId, &xHigherPriorityTaskWoken))
				{
					uart_queue_push("BME280 stop\n", 12);
				}
				break;
			case 2: 		//led
			{
				int val = atoi(string_pt + strlen(commands_tab[i]));
				if (val > 0)
				{
					if (pdPASS == xTimerChangePeriodFromISR(ledTimerId, val, &xHigherPriorityTaskWoken))
					{
						uart_queue_push("led start\n", 10);
					}
				}
				else
				{
					if (pdPASS == xTimerStopFromISR(ledTimerId, &xHigherPriorityTaskWoken))
					{
						uart_queue_push("led stop\n", 9);
						// turn off led
						HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);
					}
				}
				break;
			}
			default:
				break;
			}

			if( xHigherPriorityTaskWoken != pdFALSE )
			{
				// Call the interrupt safe yield function
				portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
			}
		}
	}
}

void uart_rxComplete(UART_HandleTypeDef *huart)
{
	// send uart message if printable char detected
	if ((rxData >= '0' && rxData <= 'z') || rxData ==' ' || rxData == '\n' || rxData == '\r')
	{
		char* cmd_pt = uart_rxProcess(rxData);
		if (cmd_pt != NULL && strlen(cmd_pt) > 0)
		{
			char buff[MAX_UART_COMMAND_LEN + 25]; 	// 25 = strlen("Uart command received: \n") + terminating null
			sprintf(buff, "Uart command received: %.*s\n", MAX_UART_COMMAND_LEN, cmd_pt);
			uart_queue_push(buff, strlen(buff));
			// TODO: cmd string should be send to the queue
			uart_cmdParsingFromISR(cmd_pt);
		}
	}
	// re-enable rx uart IT
	HAL_UART_Receive_IT(&huart2, &rxData, 1);
}

static void event_processing(void)
{
#ifdef USE_FREERTOS_EVENT
	uint32_t flags;

	flags = osEventFlagsWait(bme280EvtId,
			(BME280_FORCE_MEASSURE_EVENT | BME280_DATA_READ_EVENT),
			osFlagsWaitAny, osWaitForever);

	// Event dispatcher
	if (flags & BME280_FORCE_MEASSURE_EVENT)
	{
		  if (true == bme280_trigger())
		  {
	#ifdef USE_FREERTOS_TIMERS
			  osTimerStart(bme280SetEventDataReadTimerId, 40);
	#else
			  set_scheduled_task(40, FALSE, bme280_set_event_data_read);
	#endif
		  }
	}

	if (flags & BME280_DATA_READ_EVENT)
	{
		  struct bme280_data comp_data;

		  clear_event(BME280_DATA_READ_EVENT);
		  if (true == bme280_data_read(&comp_data))
		  {
			  print_sensor_data(&comp_data);
		  }
	}
#else
	if (check_event(BME280_FORCE_MEASSURE_EVENT))
	{
	  clear_event(BME280_FORCE_MEASSURE_EVENT);
	  if (true == bme280_trigger())
	  {
#ifdef USE_FREERTOS_TIMERS
		  osTimerStart(bme280SetEventDataReadTimerId, 40);
#else
		  set_scheduled_task(40, FALSE, bme280_set_event_data_read);
#endif
	  }
	}
	if (check_event(BME280_DATA_READ_EVENT))
	{
		struct bme280_data comp_data;

		clear_event(BME280_DATA_READ_EVENT);
		if (true == bme280_data_read(&comp_data))
		{
		  print_sensor_data(&comp_data);
		}
	}
#endif //event
}

static void queue_processing(void)
{
#ifdef USE_FREERTOS_QUEUE
#ifdef USE_STATIC_QUEUE
	uart_queue_table_t msg;
	osStatus_t status;

    status = osMessageQueueGet(uartQueue, &msg, NULL, 0U);   // wait for message
    if (status == osOK)
    {
    	//HAL_UART_Transmit(&huart2, (uint8_t *)msg.msg, msg.msg_len, 100);
    	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg.msg, msg.msg_len);
    	osThreadSuspend(uartTaskHandle);
    }
#else
#ifdef USE_FREERTOS_MEMPOOL
	uart_queue_table_t *msg_pt;
#else //USE_FREERTOS_MEMPOOL
	uart_queue_dyn_table_t *msg_pt;
#endif //USE_FREERTOS_MEMPOOL

	osStatus_t status;

    status = osMessageQueueGet(uartQueue, &msg_pt, NULL, 0U);   // wait for message
    if (status == osOK)
    {
    	//HAL_UART_Transmit(&huart2, (uint8_t *)msg.msg, msg.msg_len, 100);
    	HAL_UART_Transmit_IT(&huart2, (uint8_t *)msg_pt->msg, msg_pt->msg_len);
    	osThreadSuspend(uartTaskHandle);
#ifdef USE_FREERTOS_MEMPOOL
    	osStatus status = osMemoryPoolFree(MemPoolId, msg_pt);    // free memory block
    	if (status != osOK)
    	{
    		//assert
    	}
#else
    	vPortFree(msg_pt);
#endif //USE_FREERTOS_MEMPOOL
    }

#endif //USE_STATIC_QUEUE
#else
	// uart_queue processing
	if (uartTxState == UART_IDLE)
	{
		if (uart_queue_get_data() != NULL)
		{
		  // take from queue and send to uart -> HAL_UART_Transmit_IT(&huart2, (uint8_t *)buf, strlen(buf));
		  uint8_t* msg = (uint8_t*) uart_queue_get_data();
		  uint16_t msg_len = uart_queue_get_data_len();
		  // send to terminal
		  HAL_UART_Transmit_IT(&huart2, msg, msg_len);
		  uartTxState = UART_TRANSMIT;
		}
	}
	if (uartTxState == UART_TRANSMITED)
	{
	  uart_queue_pop();
	  uartTxState = UART_IDLE;
	}
#endif	//USE_FREERTOS_QUEUE
}

static void uartTask(void *argument)
{
	/* Infinite loop */
	for(;;)
	{
		queue_processing();
		osDelay(1);
	}
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_USART2_UART_Init();
  MX_TIM6_Init();
  /* USER CODE BEGIN 2 */
  // register callback for i2c xfer
  HAL_I2C_RegisterCallback(&hi2c1, HAL_I2C_MEM_TX_COMPLETE_CB_ID, i2c1_txComplete);
  HAL_I2C_RegisterCallback(&hi2c1, HAL_I2C_MEM_RX_COMPLETE_CB_ID, i2c1_rxComplete);
  // register tx callback for uart
  HAL_UART_RegisterCallback(&huart2, HAL_UART_TX_COMPLETE_CB_ID, uart_txComplete);
  // register rx callback for uart
  HAL_UART_RegisterCallback(&huart2, HAL_UART_RX_COMPLETE_CB_ID, uart_rxComplete);
  // enable rx uart ISR
  HAL_UART_Receive_IT(&huart2, &rxData, 1);

#ifndef USE_FREERTOS_TIMERS
  // register scheduler callback
  HAL_TIM_RegisterCallback(&htim6, HAL_TIM_PERIOD_ELAPSED_CB_ID, scheduler);
  // start scheduler
  HAL_TIM_Base_Start_IT(&htim6);
#endif
  // bme280 init
  if (BME280_OK != main_bme280_init())
  {
	  return -1;
  }

  /* USER CODE END 2 */

  /* Init scheduler */
  osKernelInitialize();

  /* USER CODE BEGIN RTOS_MUTEX */
  /* add mutexes, ... */
  /* USER CODE END RTOS_MUTEX */

  /* USER CODE BEGIN RTOS_SEMAPHORES */
  /* add semaphores, ... */
  /* USER CODE END RTOS_SEMAPHORES */

  /* USER CODE BEGIN RTOS_TIMERS */
  /* start timers, add new ones, ... */
#ifdef USE_FREERTOS_TIMERS
	// definition of data read timer - triggered once
	bme280SetEventDataReadTimerId = osTimerNew(bme280_set_event_data_read, osTimerOnce, NULL, NULL);

	// definition of trigger measure timer - triggered periodical
	bme280SetEventTriggerTimerId = osTimerNew(bme280_set_event_trigger, osTimerPeriodic, NULL, NULL);
	//osTimerStart(bme280SetEventTriggerTimerId, 1000);

	ledTimerId = osTimerNew(ledTimerCb, osTimerPeriodic, NULL, NULL);
	osTimerStart(ledTimerId, 100);
#else
	// scheduling user task
	set_scheduled_task(1000, TRUE, bme280_set_event_trigger);
	// scheduling led blink
	set_scheduled_task(100, TRUE, led_blink);
#endif

  /* USER CODE END RTOS_TIMERS */

  /* USER CODE BEGIN RTOS_QUEUES */
  /* add queues, ... */
#ifdef USE_FREERTOS_QUEUE
#ifdef USE_STATIC_QUEUE
	uartQueue = osMessageQueueNew(UART_QUEUE_TABLE_SIZE, sizeof(uart_queue_table_t), NULL);
#else
	uartQueue = osMessageQueueNew(UART_QUEUE_TABLE_SIZE, sizeof(void *), NULL);
#ifdef USE_FREERTOS_MEMPOOL
	MemPoolId = osMemoryPoolNew(MEMPOOL_OBJECTS, sizeof(uart_queue_table_t), NULL);
	if (MemPoolId == NULL)
	{
		return -1;
	}
#endif //USE_FREERTOS_MEMPOOL
#endif //USE_STATIC_QUEUE
	if (uartQueue == NULL)
	{
		return -1; // Message Queue object not created, handle failure
	}
#endif //USE_FREERTOS_QUEUE
  /* USER CODE END RTOS_QUEUES */

  /* Create the thread(s) */
  /* creation of defaultTask */
  defaultTaskHandle = osThreadNew(StartDefaultTask, NULL, &defaultTask_attributes);

  /* USER CODE BEGIN RTOS_THREADS */
  /* add threads, ... */
  uartTaskHandle = osThreadNew(uartTask, NULL, &uartTask_attributes);

  /* USER CODE END RTOS_THREADS */

  /* USER CODE BEGIN RTOS_EVENTS */
  /* add events, ... */
#ifdef USE_FREERTOS_EVENT
	 bme280EvtId = osEventFlagsNew(NULL);
#endif

  /* USER CODE END RTOS_EVENTS */

  /* Start scheduler */
  osKernelStart();

  /* We should never get here as control is now taken by the scheduler */
  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {

    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_I2C1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x00000001;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /** I2C Fast mode Plus enable
  */
  __HAL_SYSCFG_FASTMODEPLUS_ENABLE(I2C_FASTMODEPLUS_I2C1);
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 32000 - 1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 2 - 1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 38400;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(LD2_GPIO_Port, LD2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin : B1_Pin */
  GPIO_InitStruct.Pin = B1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_FALLING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(B1_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pin : LD2_Pin */
  GPIO_InitStruct.Pin = LD2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(LD2_GPIO_Port, &GPIO_InitStruct);

  /* EXTI interrupt init*/
  HAL_NVIC_SetPriority(EXTI15_10_IRQn, 5, 0);
  HAL_NVIC_EnableIRQ(EXTI15_10_IRQn);

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/* USER CODE BEGIN Header_StartDefaultTask */
/**
  * @brief  Function implementing the defaultTask thread.
  * @param  argument: Not used
  * @retval None
  */
/* USER CODE END Header_StartDefaultTask */
void StartDefaultTask(void *argument)
{
  /* USER CODE BEGIN 5 */

	/* Infinite loop */
	for(;;)
	{
		event_processing();

		osDelay(1);
		//osThreadYield()
	}
  /* USER CODE END 5 */
}

 /**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM7 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM7) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
