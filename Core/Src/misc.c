/*
 * misc.c
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */
#include "stdint.h"

volatile uint8_t SR_reg;               /* Current value of the FAULTMASK register */
volatile uint8_t SR_lock = 0x00U;      /* Lock */
