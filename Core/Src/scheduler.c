/*
 * scheduler.c
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */
#include "scheduler.h"
#include "misc.h"

#define MAX_SCHEDULED_TASK	10
/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

typedef struct task_type_tag{
	uint16_t interval;
	uint16_t tick_counter;
	bool	 is_cyclical;
	void (*callback)(void);
} task_t;
/* Private variable -----------------------------------------------------------*/

volatile task_t task_v[MAX_SCHEDULED_TASK];

/* Functions -----------------------------------------------------------*/

bool set_scheduled_task(uint16_t interval, bool	is_cyclical, void (*callback)(void))
{
	for (uint8_t task_index = 0; task_index < MAX_SCHEDULED_TASK; task_index++)
	{
		if (task_v[task_index].callback == NULL)
		{
			// disable interrupt
			EnterCritical();
			task_v[task_index].interval = interval;
			task_v[task_index].is_cyclical = is_cyclical;
			task_v[task_index].callback = callback;
			task_v[task_index].tick_counter = interval;
			// enable interrupt
			ExitCritical();
			return true;
		}
	}
	return false;
}

void scheduler(TIM_HandleTypeDef *htim)
{
	for (uint8_t task_index = 0; task_index < MAX_SCHEDULED_TASK; task_index++)
	{
		if (task_v[task_index].callback != NULL)
		{
			if (task_v[task_index].tick_counter > 0)
			{
				task_v[task_index].tick_counter--;
			}
			if (task_v[task_index].tick_counter == 0)
			{
				task_v[task_index].callback();
				if (task_v[task_index].is_cyclical == true)
				{
					task_v[task_index].tick_counter = task_v[task_index].interval;
				}
				else
				{
					task_v[task_index].callback = NULL;
				}
			}
		}
	}
}


