/*
 * queues.c
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#include "main.h"
#include "stdint.h"
#include "stdbool.h"
#include "stdlib.h"
#include "string.h"
#include "cmsis_os.h"
#include "queues.h"

#ifdef USE_FREERTOS_QUEUE
extern osMessageQueueId_t uartQueue;
#endif
#ifdef USE_FREERTOS_MEMPOOL
extern osMemoryPoolId_t MemPoolId;
#endif //USE_FREERTOS_MEMPOOL

/* USER CODE BEGIN PTD */

typedef struct uart_queue_element_s {
	char* 							msg;
	uint16_t						msg_len;
	struct uart_queue_element_s* 	next;
} uart_queue_element_t;

typedef struct uart_queue_s {
	uart_queue_element_t* head;
	uart_queue_element_t* tail;
}uart_queue_t;

typedef struct uart_queue_table_ind_s {
	uint16_t head;
	uint16_t tail;
}uart_queue_table_ind_t;

/* USER CODE BEGIN PV */

#ifdef USE_STATIC_QUEUE
// static queue variable
uart_queue_table_t uart_queue_table[UART_QUEUE_TABLE_SIZE];
uart_queue_table_ind_t uart_queue_table_ind = {
		.head = 0,
		.tail = 0,
};
#else
// dynamic allocated queue variable
uart_queue_t uart_queue = { NULL, NULL };
#endif

/* Functions ----------------------------------------------------------*/
#ifdef USE_STATIC_QUEUE

static bool uart_queue_table_push(char* msg_pt, uint16_t msg_len)
{
	if (((uart_queue_table_ind.tail + 1) & (UART_QUEUE_TABLE_SIZE - 1))
			!= uart_queue_table_ind.head)
	{
		// copy msg to current tail
		memcpy(uart_queue_table[uart_queue_table_ind.tail].msg, msg_pt, msg_len);
		uart_queue_table[uart_queue_table_ind.tail].msg_len = msg_len;

		// move tail
		uart_queue_table_ind.tail = ((uart_queue_table_ind.tail + 1) & (UART_QUEUE_TABLE_SIZE - 1));
		return true;
	}
	return false;
}
#else

#ifndef USE_FREERTOS_QUEUE

static bool uart_queue_malloc_push(char* msg_pt, uint16_t msg_len)
{
	// allocate queue element
	uart_queue_element_t* new_elem = malloc(sizeof(uart_queue_element_t));
	if (new_elem != NULL)
	{
		char* temp = malloc(msg_len);
		if (temp != NULL)
		{
			// copy msg
			memcpy(temp, msg_pt, msg_len);
			// point to msg
			new_elem->msg = temp;
			new_elem->msg_len = msg_len;
		}
		else
		{
			free(new_elem);
			return false;
		}
		// next always is null
		new_elem->next = NULL;

		if (uart_queue.head == NULL)
		{
			// add first element
			uart_queue.head = new_elem;
			uart_queue.tail = new_elem;
		}
		else
		{
			uart_queue.tail->next = new_elem;
			uart_queue.tail = new_elem;
		}
	}
	return false;
}
#endif //USE_FREERTOS_QUEUE
#endif

#ifdef USE_STATIC_QUEUE
static bool uart_queue_table_pop(void)
{
	if (uart_queue_table_ind.head == uart_queue_table_ind.tail)
	{
		return false;
	}
	else
	{
		uart_queue_table_ind.head = ((uart_queue_table_ind.head + 1) & (UART_QUEUE_TABLE_SIZE - 1));
		return true;
	}
}
#else
static bool uart_queue_malloc_pop(void)
{
	if (uart_queue.head != NULL)
	{
		uart_queue_element_t* temp = uart_queue.head;
		// point head to next elenment
		uart_queue.head = temp->next;
		// free allocated memory
		if (temp->msg != NULL)
		{
			free(temp->msg);
		}
		free(temp);

		return true;
	}
	return false;
}
#endif

#ifdef USE_STATIC_QUEUE
static uart_queue_table_t* uart_queue_table_get_element(void)
{
	if (uart_queue_table_ind.head == uart_queue_table_ind.tail)
	{
		return NULL;
	}
	else
	{
		return &uart_queue_table[uart_queue_table_ind.head];
	}
}
#else
static uart_queue_element_t* uart_queue_malloc_get_element(void)
{
	return uart_queue.head;
}
#endif

/* public functions -----------------------------------------------------------*/
bool uart_queue_push(char* msg_pt, uint16_t msg_len)
{
#ifdef USE_FREERTOS_QUEUE
	osStatus_t status = osErrorNoMemory;
#ifdef USE_STATIC_QUEUE
	uart_queue_table_t msg;

	if (msg_len > sizeof(msg.msg))
	{
		msg_len = sizeof(msg.msg);
	}
	memcpy(msg.msg, msg_pt, msg_len);
	msg.msg_len = msg_len;
	status = osMessageQueuePut(uartQueue, &msg, 0U, 0U);
	return (status == osOK);
#else //USE_STATIC_QUEUE
#ifdef USE_FREERTOS_MEMPOOL
	uart_queue_table_t *msg_local_pt;

	if (msg_len > sizeof(msg_local_pt->msg))
	{
		msg_len = sizeof(msg_local_pt->msg);
	}
	msg_local_pt = (uart_queue_table_t *)osMemoryPoolAlloc(MemPoolId, 0U);  // get Mem Block
#else //USE_FREERTOS_MEMPOOL
	uart_queue_dyn_table_t *msg_local_pt;
	msg_local_pt = pvPortMalloc(sizeof(uart_queue_dyn_table_t) + msg_len);
#endif //USE_FREERTOS_MEMPOOL

	if (msg_local_pt != NULL)
	{
		memcpy(msg_local_pt->msg, msg_pt, msg_len);
		msg_local_pt->msg_len = msg_len;
		status = osMessageQueuePut(uartQueue, &msg_local_pt, 0U, 0U);
	}
	return (status == osOK);
#endif //USE_STATIC_QUEUE
#else //USE_FREERTOS_QUEUE
#ifdef USE_STATIC_QUEUE
	return uart_queue_table_push(msg_pt, msg_len);
#else //USE_STATIC_QUEUE
	return uart_queue_malloc_push(msg_pt, msg_len);
#endif //USE_STATIC_QUEUE
#endif //USE_FREERTOS_QUEUE
}

bool uart_queue_pop(void)
{
#ifdef USE_STATIC_QUEUE
	return uart_queue_table_pop();
#else
	return uart_queue_malloc_pop();
#endif
}

uint8_t* uart_queue_get_data(void)
{
#ifdef USE_STATIC_QUEUE
	uart_queue_table_t* get_pt = uart_queue_table_get_element();
	return (uint8_t*)get_pt->msg;
#else
	uart_queue_element_t* get_pt = uart_queue_malloc_get_element();
	if (get_pt != NULL)
		return (uint8_t*)get_pt->msg;
	else
		return NULL;
#endif
}

uint16_t uart_queue_get_data_len(void)
{
#ifdef USE_STATIC_QUEUE
	uart_queue_table_t* get_pt = uart_queue_table_get_element();
	return get_pt->msg_len;
#else
	uart_queue_element_t* get_pt = uart_queue_malloc_get_element();
	if (get_pt != NULL)
		return get_pt->msg_len;
	else
		return 0;
#endif
}
