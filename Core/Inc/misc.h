/*
 * misc.h
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#ifndef APPLICATION_USER_MISC_H_
#define APPLICATION_USER_MISC_H_

#include "stdint.h"

extern volatile uint8_t SR_reg;               /* Current value of the FAULTMASK register */
extern volatile uint8_t SR_lock;		      /* Lock */

/* Save status register and disable interrupts */
#define EnterCritical() \
  do {\
    if (++SR_lock == 1u) {\
      asm ( \
      "MRS R0, PRIMASK\n\t" \
      "CPSID i\n\t"            \
      "STRB R0, %[output]"  \
      : [output] "=m" (SR_reg)\
      :: "r0");\
	}\
  } while(0)

/* Restore status register  */
#define ExitCritical() \
  do {\
    if (--SR_lock == 0u) { \
      asm (                 \
      "ldrb r0, %[input]\n\t"\
      "msr PRIMASK,r0;\n\t" \
      ::[input] "m" (SR_reg)  \
      : "r0");                \
    }\
  } while(0)

#endif /* APPLICATION_USER_MISC_H_ */
