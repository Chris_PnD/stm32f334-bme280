/*
 * queues.h
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */
#include "stdint.h"

#ifndef APPLICATION_USER_QUEUES_H_
#define APPLICATION_USER_QUEUES_H_

#define QUEUE_MAX_MSG_LEN 			45
#define UART_QUEUE_TABLE_SIZE		(1 << 4)

typedef struct uart_queue_table_s {
	char 		msg[QUEUE_MAX_MSG_LEN];
	uint16_t	msg_len;
}uart_queue_table_t;

typedef struct uart_queue_dyn_table_s {
	uint16_t	msg_len;
	char 		msg[];
}uart_queue_dyn_table_t;


bool uart_queue_push(char* msg_pt, uint16_t msg_len);
bool uart_queue_pop(void);
uint8_t* uart_queue_get_data(void);
uint16_t uart_queue_get_data_len(void);

#endif /* APPLICATION_USER_QUEUES_H_ */
