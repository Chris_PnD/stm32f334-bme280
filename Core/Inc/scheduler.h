/*
 * scheduler.h
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#ifndef APPLICATION_USER_SCHEDULER_H_
#define APPLICATION_USER_SCHEDULER_H_

#include "stdint.h"
#include "stdbool.h"
#include "stm32f3xx_hal.h"

bool set_scheduled_task(uint16_t interval, bool	is_cyclical, void (*callback)(void));
void scheduler(TIM_HandleTypeDef *htim);

#endif /* APPLICATION_USER_SCHEDULER_H_ */
