/*
 * bme280_middleware.h
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#ifndef APPLICATION_USER_BME280_MIDDLEWARE_H_
#define APPLICATION_USER_BME280_MIDDLEWARE_H_

#include "../../Drivers/BME280_driver/bme280.h"

void bme280_delay_us(uint32_t period, void *intf_ptr);
BME280_INTF_RET_TYPE bme280_read(uint8_t reg_addr, uint8_t *reg_data, uint32_t len, void *intf_ptr);
BME280_INTF_RET_TYPE bme280_write(uint8_t reg_addr, const uint8_t *reg_data, uint32_t len,
                                                    void *intf_ptr);

void print_sensor_data(struct bme280_data *comp_data_pt);
int8_t main_bme280_init(void);
bool bme280_trigger(void);
bool bme280_data_read(struct bme280_data *comp_data_pt);

#ifdef USE_FREERTOS_TIMERS
void bme280_set_event_trigger(void *argument);
void bme280_set_event_data_read(void *argument);
#else
void bme280_set_event_trigger(void);
void bme280_set_event_data_read(void);
#endif

void i2c1_rxComplete(I2C_HandleTypeDef *hi2c);
void i2c1_txComplete(I2C_HandleTypeDef *hi2c);

#endif /* APPLICATION_USER_BME280_MIDDLEWARE_H_ */
