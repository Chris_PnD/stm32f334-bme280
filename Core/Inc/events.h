/*
 * events.h
 *
 *  Created on: 28 kwi 2021
 *      Author: chris
 */

#ifndef APPLICATION_USER_EVENTS_H_
#define APPLICATION_USER_EVENTS_H_

#define BME280_FORCE_MEASSURE_EVENT (1 << 0)
#define BME280_DATA_READ_EVENT 		(1 << 1)

void set_event(uint8_t event_type);
void clear_event(uint8_t event_type);
bool check_event(uint8_t event_type);

#endif /* APPLICATION_USER_EVENTS_H_ */
